Logging System
=====

One of the most important parts of any system is **logs**. It helps to detect issues, bugs or crashes. Also, it helps to explain the system performance and the expected changes.

#### THE CHALLENGE
Create simple logging system which should have an API endpoints to send logs and dashboard to view those logs.

> The log object should contain a log title, description, status code, log path and created at date.

###### PART 1: API Endpoints
> Create 3 API endpoints to create, view, and list logs.
1. [POST] `api/log` to create log.
2. [GET] `api/log` to view logs.
3. [GET] `api/log/{id}` to get log details.

###### PART 2: Simple Admin Dashboard
> Create a simple admin dashboard to view logs in a table view. 
1. Make it paginated and view it in a descending way.
2. Make it searchable by title, description, and status code.

#### RESTRICTIONS
* Use any NodeJS framework (preferably hapijs) for the backend part.
* Use Angular 6+ framework and bootstrap for the frontend part.
* Setup a gitlab repository on your personal gitlab account that will contain the
  whole project.
  
#### CRITERIA
> The code will be judged on the following criteria:
1. Strategy for building the system structure.
2. Readability and inline documentation (preferably code should explain itself and
not need additional comments).
3. The project should be run by docker-compose up.
4. README file to setup working solution.
5. The system should be functional and complete.
6. **[Bonus]** Use swagger API documentation for your API design.
7. **[Bonus]** Tests and code coverage of at least the API endpoints.
8. **[Bonus]** Add GitLab compatible CI/CD to perform a server deploy.